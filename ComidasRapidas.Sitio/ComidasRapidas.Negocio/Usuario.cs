﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;

namespace ComidasRapidas.Negocio
{
    public class Usuario
    {

        private int id_usuario;
        private string nombre_usuario;
        private string apellidoP_usuario;
        private string apellidoM_usuario;
        private string direccion_usuario;
        private int telefono_usuario;    
        private string usuario_usuario;       
        private string clave_usuario;

        public Usuario() { }

        public Usuario(string nombre, string apellidoP,string apellidoM, string direccion, int telefono, string usuario, string contrasenia)
        {

            nombre_usuario = nombre;
            apellidoP_usuario = apellidoP;
            apellidoM_usuario = apellidoM;
            direccion_usuario = direccion;
            telefono_usuario = telefono;
            usuario_usuario= usuario;
            clave_usuario= contrasenia;
        }
        
        public int registrar_usuario() 
        {
            List<MySqlParameter> lista = new List<MySqlParameter>();
            string sql = "insert into usuario(id_usuario,nombre_usuario, apellidoP_usuario,apellidoM_usuario, direccion_usuario, telefono_usuario,usuario, clave, fk_idrol) values({0},'{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')";
            sql = string.Format(sql,id_usuario, nombre_usuario, apellidoP_usuario,apellidoM_usuario,direccion_usuario,telefono_usuario, usuario_usuario, clave_usuario);
            return Conexion.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);        
        }

        public DataTable consultar_usuario(string usuario) 
        {
            List<MySqlParameter> lista = new List<MySqlParameter>();
            string sql = "select per.nombre_persona, per.apellido_persona, per.id_persona, r.id_rol from persona per"
                +" inner join rol r"
                +" on r.id_rol=per.fk_idrol"
                +" where per.usuario_persona='"+usuario+"'";
            return Conexion.EjecutarConsulta(sql, lista, System.Data.CommandType.Text);
                 
        }

        public DataTable consultar_pws(string usuario)
        {
            List<MySqlParameter> lista = new List<MySqlParameter>();
            string sql = "select psw_persona from persona where usuario_persona='"+usuario+"'";
            return Conexion.EjecutarConsulta(sql, lista, System.Data.CommandType.Text);
        }

        public int modificar_persona() 
        {
            return 1;
        }

        public int eliminar_persona(int id_persona) 
        {
            return 1;
        
        }

    }
}
