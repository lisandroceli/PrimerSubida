﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;

namespace ComidasRapidas.Negocio
{
    public class menu_sesion
    {
        private string nombre_menu;
        private string url_menu;

        public menu_sesion() { }

        public DataTable consultar_menu() 
        {
            List<MySqlParameter> lista = new List<MySqlParameter>();
            string sql = "select *from menu_sesion";
            return Conexion.EjecutarConsulta(sql, lista, System.Data.CommandType.Text);         
        }

        public DataTable consultar_menu_sesion(int id_rol) 
        {
            List<MySqlParameter> lista = new List<MySqlParameter>();
            string sql = "select m.id_menu, m.nombre_menu, m.url_menu from menu_sesion m"
                + " inner join menu_rol mr"
                + " on m.id_menu=mr.fk_idmenu"
                + " inner join rol r"
                + " on r.id_rol=mr.fk_idrol"
                + " where mr.fk_idrol="+id_rol;
            return Conexion.EjecutarConsulta(sql, lista, System.Data.CommandType.Text);         
        }
    }
}
