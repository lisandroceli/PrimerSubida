﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace ComidasRapidas.Negocio
{
    public class Producto
    {
        private string nombre_producto;
        private string descripcion_producto;
        private string imagen_producto;
        private int precio_producto;
        private int fk_id_negocio;

        public Producto() 
        {
        
        }

        public Producto(string nombre, string descripcion, string imagen, int precio, int fk_negocio) 
        {
            nombre_producto = nombre;
            descripcion_producto = descripcion;
            imagen_producto = imagen;
            precio_producto = precio;
            fk_id_negocio = fk_negocio;
        }

        public int Guardar_producto() 
        {
            return 1;        
        }

        public DataTable Consultar_producto(int id_producto) 
        {
            DataTable dt = new DataTable();
            return dt;        
        }

        public DataTable Mostrar_todos() 
        {
            DataTable dt = new DataTable();
            return dt; 
        
        }

        public int Modificar_producto() 
        {
            return 1;
        
        }

        public int Eliminar_producto() 
        {
            return 1;        
        }
        

    }
}
