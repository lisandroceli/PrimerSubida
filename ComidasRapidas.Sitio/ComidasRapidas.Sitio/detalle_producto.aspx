﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="detalle_producto.aspx.cs" Inherits="ComidasRapidas.detalle_producto" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8" />
    <title>Menu</title>
    <link rel="stylesheet" href="estilos/menu.css" />
    <link rel="stylesheet" href="estilos/detalle_producto.css" />

    <script src="http://code.jquery.com/jquery-latest.js"></script>
</head>
<header>
    <div class="wrapper">
        <div class="logo">ComidasRápidas</div>

        <nav>
            <a href="index.aspx">Inicio</a>
            <a href="registrar_cliente.aspx">Registrarme</a>
            <a href="iniciar_sesion.aspx">Iniciar Sesión</a>
             <a href="contacto.aspx">Contacto</a>
        </nav>
    </div>

</header>
<body style="background: #ededed">

    <form id="form1" runat="server">
       
    <div class="general">
		
		<div class="negocios">
			<div class="titulo">
			<h1>Restaurante
                </h1>	
			</div>		
            <div class="contenedor_grid">
                <asp:GridView ID="tabla_negocios" CssClass="grid" runat="server" ShowHeader="False"   GridLines="Horizontal"                
                    >
                    <Columns>
                        <asp:ImageField DataImageUrlField="Imagen" DataImageUrlFormatString="~/imagenegocio/{0}" HeaderText="Imagen">
                        </asp:ImageField>
                        <asp:BoundField DataField="descripcion" HeaderText="Descripcion|" SortExpression="descripcion" />
                    </Columns>
                </asp:GridView>
            </div>
		</div>

        <div class="productos">
            <div class="titulo_productos">

                <h2>Productos
                </h2>	
                <div class="contenedor_grid">
                <asp:GridView ID="GridView1" CssClass="grid" runat="server" ShowHeader="False"   GridLines="Horizontal"                
                    >
                    <Columns>
                        <asp:ImageField DataImageUrlField="Imagen" DataImageUrlFormatString="~/imagenegocio/{0}" HeaderText="Imagen">
                        </asp:ImageField>
                        <asp:BoundField DataField="descripcion" HeaderText="Descripcion|" SortExpression="descripcion" />
                    </Columns>
                </asp:GridView>
            </div>
            </div>

        </div>
        <div class="div_carrito">
            <div class="titulo_productos">
                <h2>Tu pedido:</h2>
            </div>

        </div>
	</div>     
        <div class="footer">
            <div class="footer_hijo">
                Copyright 2015 - Desarrollado por Lisandro Celis
            </div>
            
        </div>
    </form>
</body>
</html>
