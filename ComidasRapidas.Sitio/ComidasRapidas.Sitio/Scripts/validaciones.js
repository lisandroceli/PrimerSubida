﻿function validacion(input) {

    var tamanio = input.value;

    var num = 7;
    
    if (tamanio.length<num || tamanio.length>num) {
        
        input.setCustomValidity("El número telefonico debe tener 7 digitos");
    } 
    else {
        input.setCustomValidity('');       
    }
}



function soloNumeros(e) {
    var key = window.Event ? e.which : e.keyCode
    return ((key >= 48 && key <= 57) || (key == 8))
}

function validarEmail(email) {
    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!expr.exec(email.value)) {
        email.setCustomValidity("Dirección de correo electrónico incorrecta");
    } else
    {
        email.setCustomValidity('');
    }

}


 
