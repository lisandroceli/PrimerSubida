﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="registrar_admin_negocio.aspx.cs" Inherits="ComidasRapidas.paginas.registrar_admin_negocio" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8" />
    <title>Menu</title>
    <link rel="stylesheet" href="../estilos/menu.css" />   
     <script src="../Scripts/validaciones.js"></script> 
    <link rel="stylesheet" href="../estilos/registrar_admin_negocio.css" />   
    <script src="http://code.jquery.com/jquery-latest.js"></script>
</head>
<header>
    <div class="wrapper">
        <div class="logo">ComidasRápidas</div>

        <nav>
            <a href="index.aspx">Inicio</a>
            <a href="registrar_cliente.aspx">Registrarme</a>
            <a href="iniciar_sesion.aspx">Iniciar Sesión</a>
             <a href="contacto.aspx">Contacto</a>
        </nav>
    </div>

</header>
<body style="background: #ededed">
    <form id="form1" runat="server">
    <div class="general"  style="background: #ededed">
        <div class="centrado">
             <h1> Usuario nuevo</h1>
            <asp:TextBox ID="txt_cedula" CssClass="textbox" required="required" onKeyPress="return soloNumeros(event)" placeholder="Cédula" runat="server"></asp:TextBox>
            <asp:TextBox ID="txt_nombre" CssClass="textbox" placeholder="Nombre" required="required" runat="server"></asp:TextBox>
             <asp:TextBox ID="txt_apellido" CssClass="textbox"  placeholder="Apellido" required="required" runat="server"></asp:TextBox>
            <asp:TextBox ID="txt_direccion" CssClass="textbox"  placeholder="Dirección" required="required" runat="server"></asp:TextBox>
            <asp:TextBox ID="txt_telefono" CssClass="textbox" onKeyPress="return soloNumeros(event)" onblur="validacion(this)"  placeholder="Teléfono" required="required" runat="server"></asp:TextBox>
            <asp:TextBox ID="txt_usuario" CssClass="textbox"  placeholder="Usuario" required="required" runat="server"></asp:TextBox>
            <asp:TextBox ID="txt_contrasenia" placeholder="Contraseña" Type="password" required="required" CssClass="textbox" runat="server"></asp:TextBox>
            <asp:TextBox ID="txt_repetir_contrasenia" Type="password" CssClass="textbox" required="required" placeholder="Confirmar Contraseña" runat="server"></asp:TextBox>
            <asp:Button ID="btn_ingresar" CssClass="boton" runat="server" Text="Registrar" OnClick="btn_ingresar_Click" />      
        </div>
    </div>
        <div class="footer">
            <div class="footer_hijo">
                Copyright 2015 - Desarrollado por Lisandro Celis
            </div>
        </div>
    </form>
</body>
</html>
