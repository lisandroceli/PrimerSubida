﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="registrar_cliente.aspx.cs" Inherits="ComidasRapidas.registrar_cliente" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8" />
    <title>Menu</title>
    <link rel="stylesheet" href="estilos/menu.css" />   
    <link rel="stylesheet" href="estilos/registrar_cliente.css" /> 
       <script src="../Scripts/validaciones.js"></script>   
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="../Scripts/validaciones.js"></script> 
    <script>
        function validarcontrasenia() {

            var psw1 = document.getElementById('txt_contrasenia');

            var psw2 = document.getElementById('txt_repetir_contrasenia');

            if (psw1.value != psw2.value) {
                
                txt_repetir_contrasenia.setCustomValidity("Las contraseñas no coinciden");
            }
            else{
               
                txt_repetir_contrasenia.setCustomValidity('');
            }
        }
    </script>
</head>
<header>
    <div class="wrapper">
        <div class="logo">ComidasRápidas</div>

        <nav>
            <a href="index.aspx">Inicio</a>
            <a href="registrar_cliente.aspx">Registrarme</a>
            <a href="iniciar_sesion.aspx">Iniciar Sesión</a>
             <a href="contacto.aspx">Contacto</a>
        </nav>
    </div>

</header>
<body style="background: #ededed">
    <form id="form1" runat="server">
    <div class="general" style="background: #ededed">
        <div class="centrado">
             <h1> Usuario Nuevo</h1>

            <asp:TextBox ID="txt_nombre" CssClass="textbox" required="required" placeholder="Nombre" runat="server"></asp:TextBox>

             <asp:TextBox ID="txt_apellido" CssClass="textbox" required="required"  placeholder="Apellido" runat="server"></asp:TextBox>
            <asp:TextBox ID="txt_email" CssClass="textbox" required="required" placeholder="Email" onblur="validarEmail(this)" runat="server"></asp:TextBox>
            <asp:TextBox ID="txt_contrasenia" placeholder="Contraseña" required="required" Type="password" onblur="validarcontrasenia()" CssClass="textbox" runat="server"></asp:TextBox>
            <asp:TextBox ID="txt_repetir_contrasenia" Type="password" required="required" CssClass="textbox" onblur="validarcontrasenia()" placeholder="Confirmar Contraseña" runat="server"></asp:TextBox>
            <asp:Button ID="btn_ingresar" CssClass="boton" runat="server" Text="Registrarme" OnClick="btn_ingresar_Click" />      
        </div>       

    </div>
        <div class="footer">
            <div class="footer_hijo">
                Copyright 2015 - Desarrollado por Lisandro Celis
            </div>            
        </div>
    </form>
</body>
</html>
