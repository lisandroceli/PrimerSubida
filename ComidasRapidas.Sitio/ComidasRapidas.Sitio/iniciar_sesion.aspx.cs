﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using ComidasRapidas.Negocio;

namespace ComidasRapidas
{
    public partial class iniciar_sesion : System.Web.UI.Page
    {
        static DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_ingresar_Click(object sender, EventArgs e)
        {
            if (txt_usuario.Text != null & txt_contrasenia.Text != null)
            {
                Usuario per = new Usuario();
                dt = per.consultar_pws(txt_usuario.Text);

                if (dt.Rows.Count > 0)
                {
                    try
                    {
                        dt = per.consultar_persona(txt_usuario.Text);
                        Session["usuario"] = dt.Rows[0][0].ToString() + " " + dt.Rows[0][1].ToString();                        
                        Session["id_user"] = dt.Rows[0][2].ToString();
                        Session["id_rol"] = dt.Rows[0][3].ToString();
                        Session["usuario_user"] = txt_usuario.Text;
                        Response.Redirect("sesion.aspx");

                    }
                    catch (Exception exe)
                    {

                    }

                }
                else 
                {
                    string script = "visible()";
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "popup", script, true) ;                
                }

            }
            else 
            {
                           
            
            }           

        }
    }
}